<?php

namespace Invian\RBAC\Testing;

use Tests\TestCase;

class ExampleRbacTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
