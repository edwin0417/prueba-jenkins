<?php

namespace Invian\Base\Testing;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleBaseTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
